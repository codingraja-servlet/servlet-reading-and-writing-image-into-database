package com.codingraja.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.codingraja.db.Database;

@MultipartConfig(maxFileSize = 1024 * 1024 * 1)
@WebServlet("/ImageUploadServlet")
public class ImageUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Connection connection = Database.getConnection();
	
	public ImageUploadServlet() {
		// Do Nothing
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("index.html").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		// Getting File data
		Part part = request.getPart("fileData");

		// Getting Image Name
		String imageName = part.getSubmittedFileName();
		
		if(validateImage(imageName)){
			//Getting Image Bytes
			InputStream in = part.getInputStream();
	
			long imageId = writeBytesIntoDatabase(imageName, in);
	
			if(imageId > 0) {
				out.println("<h1 style=\"text-align:center;\">"
							+"Image has been stored successfully!"
							+ "</h1>");
				out.println("<h3 style=\"text-align:center;\"><a "
							+"href=\"DisplayImageServlet?imageId="+imageId
							+"\">Display Image</a></h3>");
			}else{
				out.print("<h1>Some Database Exception has been occurred!</h1>");
			}
		}else {
			out.print("<h1>Invalid Image Formate, Select (.jpg, .png, .gif)!</h1>");
		}
	}
	
	//Private Method to write Image bytes into database
	private long writeBytesIntoDatabase(String imageName, InputStream in){
		String sql = "INSERT INTO image_master(image_name, image_data) VALUES(?,?)";
		try{
			PreparedStatement pStatement = connection.prepareStatement(sql,
								                    Statement.RETURN_GENERATED_KEYS);
			pStatement.setString(1, imageName);
			pStatement.setBlob(2, in);
			
			pStatement.executeUpdate();
			
			//Get Generated Image_id
			ResultSet rs = pStatement.getGeneratedKeys();
			if(rs.next())
				return rs.getLong(1);
				
		}catch (Exception ex) {
			return 0;
		}
		return 0;
	}
	
	//Validates uploaded file is Image or not
	private boolean validateImage(String imageName){
		String fileExt = imageName.substring(imageName.length()-3);
		if("jpg".equals(fileExt) || "png".equals(fileExt) || "gif".equals(fileExt))
			return true;
		
		return false;
	}
}
