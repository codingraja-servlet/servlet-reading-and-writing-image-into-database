package com.codingraja.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.codingraja.db.Database;
import com.codingraja.domain.Image;

@WebServlet("/DisplayImageServlet")
public class DisplayImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//Get Connection Object
	private Connection connection = Database.getConnection();
	
	public DisplayImageServlet() {
		// Do Nothing
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		long imageId = Long.parseLong(request.getParameter("imageId"));
		
		// Reading Image Bytes by passing Image ID
		Image image = readBytesFromDatabase(imageId);

		if (image == null)
			response.getWriter().println("<h1>Image Not Found Exception</h1>");
		else {
			// Get Image Name From Image Object
			String imageName = image.getImageName();

			// Get Content-Type of Image
			String contentType = this.getServletContext().getMimeType(imageName);

			// Set Content Type
			response.setHeader("Content-Type", contentType);

			// Set Content-Length of Image Data
			response.setHeader("Content-Length", String.valueOf(image.getImageData().length));

			// Set Content-Disposition
			response.setHeader("Content-Disposition", 
							   "inline; filename=\"" + image.getImageName() + "\"");
			
			//Write Image Data to Response
			response.getOutputStream().write(image.getImageData());
		}
	}

	// Private Method Implementation to read Image name and Bytes from Database
	private Image readBytesFromDatabase(long imageId) {
		String sql = "SELECT * FROM image_master WHERE image_id=?";
		try {
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setLong(1, imageId);

			ResultSet resultSet = pStatement.executeQuery();

			if (resultSet.next()) {
				Image image = new Image();
				image.setImageName(resultSet.getString(2));
				image.setImageData(resultSet.getBytes(3));
				return image;
			}
		} catch (Exception ex) {
			return null;
		}
		return null;
	}
}
