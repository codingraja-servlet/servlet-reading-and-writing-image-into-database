package com.codingraja.domain;

public class Image {
	private String imageName;
	private byte[] imageData;

	public Image() {
		// Do Nothing
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public byte[] getImageData() {
		return imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}
}
